# **PlasmidHawk**

Sequence alignment-based lab-of-origin prediction software. Scripts and results for the [publication](https://www.biorxiv.org/content/10.1101/2020.05.22.110270v1) are in the [data branch](https://gitlab.com/treangenlab/plasmidhawk/-/tree/data).

## Installation
PlasmidHawk can be installed through `conda` via `conda install -c bioconda plasmidhawk`. 

**Dependencies:** In order to run PlasmidHawk without installing via `conda`, you will need [Python 3.x](https://www.python.org/download/releases/3.0/), [biopython](https://biopython.org/), [plaster](https://gitlab.com/treangenlab/plaster), `tqdm` and [mummer-4.0.0beta2](https://github.com/mummer4/mummer/releases). Note that if installing through `conda`, you are only required to install `plasmidhawk` as the rest of the dependencies will ship with it as well.

## **Usage**

### **Step1** Pan-genome construction
```
usage: plaster [-h] [-r] [-t TEMPLATE] [-o OUTPUT] [-w WORK_DIR] [-p THREADS]
               [-l LENGTH] [-i ID_CUTOFF] [-v]
               input-files [input-files ...]

positional arguments:
  input-files           a list of input fasta file names. If there is one
                        file, it is assumed that this file contains a list of
                        input files separated by a newline

optional arguments:
  -h, --help            show this help message and exit
  -r, --realign         Realign all input genomes to the resulting pangenome
                        to get a more accurate fragment mapping
  -t TEMPLATE, --template TEMPLATE
                        seed genome to use
  -o OUTPUT, --output OUTPUT
                        output pan-genome fasta and metadata file stem (does
                        not include file extension)
  -w WORK_DIR, --work-dir WORK_DIR
                        Directory to save nucmer outputs.
  -p THREADS, --threads THREADS
                        Number of threads
  -l LENGTH, --length LENGTH
                        Minimum length of sequence attached to the pan-genome
  -i ID_CUTOFF, --id-cutoff ID_CUTOFF
                        Minimum identity to record alignment in metadata
  -v, --verbose         Print verbose output

```

**Example:**

The following command will run `plaster` on each file listed in `example/exp_input.txt` and output two files:
`example/exp_pangenome.fasta` and `example/exp_pangenome.tsv`
```
plaster example/exp_input.txt -o example/exp_pangenome -r
```

Alternatively, if you wish to run plaster on all files in some directory, you can pass in a list of files:
```
plaster example/exp_train*.fasta -o example/exp_pangenome -r
```



### **Step2** Pan-genome annotation

Next we will want to annotate each fragment with its known lab(s) of origin. The following command will output a file with the `_lab.tsv` suffix that contains a mapping from each lab to all of the fragments.

```
plasmidhawk annotate [Flags]
```


```
plasmidhawk annotate -h
usage: plasmidhawk annotate [-h] [-o OUTPUT] frag-metadata plasmid-metadata

positional arguments:
  frag-metadata         The TSV metadata generated from plaster
  plasmid-metadata      File containing information on which labs ordered
                        which plasmids

optional arguments:
  -h, --help            show this help message and exit
  -o OUTPUT, --output OUTPUT
                        output file prefix

```

**Example:**

``
plasmidhawk annotate example/exp_pangenome.tsv example/exp_lab_plasmid.txt
``

### **Step3** Lab-of-origin Prediction

We can now run the prediction mode of `plasmidhawk` and predict the source lab of input plasmids. 
```
plasmidhawk predict [Flags]
```

```
plasmidhawk predict -h
usage: plasmidhawk predict [-h] [-o OUTPUT] [-s] [-w WORK_DIR] [-t THREAD]
                           [-d IDENTITY] [-v]
                           [{max,supermax,correct}] input-pangenome-fasta
                           input-pangenome-annotated-meta input-files
                           [input-files ...]

positional arguments:
  {max,supermax,correct}
                        Choose prediction mode (max, supermax, correct),
                        default max. supermax is max mode, but output top 50
                        labs
  input-pangenome-fasta
                        input pan-genome fasta file
  input-pangenome-annotated-meta
                        Lab ownership metadata file
  input-files           a list of input fasta file names. If there is one file
                        and it ends with a non-fasta suffix it is assumed that
                        this file contains a list of input files separated by
                        a newline

optional arguments:
  -h, --help            show this help message and exit
  -o OUTPUT, --output OUTPUT
                        output lab-of-origin prediction
  -s, --skip            Use nucmer results already present in work-dir instead
                        of rerunning
  -w WORK_DIR, --work-dir WORK_DIR
                        output lab-of-origin prediction
  -t THREAD, --thread THREAD
                        Number of threads, default 20
  -d IDENTITY, --identity IDENTITY
                        Minimum alignment identity [0,100], default 0
  -v, --verbose         Print verbose output

```

**Example:**

```
plasmidhawk predict correct example/exp_pangenome.fasta example/exp_pangenome_lab.tsv example/exp_pred*.fasta -o example/correct_results.txt
```

The example should take less than 5 minutes to finish the whole process. 

## **Description**

**Pipeline**

![](image/plasmidhawk_pipeline.png)

